
# Initial Setup and Prerequisites

## Copy the .env.example to .env
If no .env exists the default values will create config and data directories in the project directory.
```
cp .env.example .env
```

## How to create a proxy network to be shared with containers

```bash
docker network create --driver=bridge --subnet=169.254.0.0/22 --gateway=169.254.0.1 proxy

```

### NOTE: Wireguard VPN
Wireguard server config goes into ROOT environment varibale path under wireguard/wg0.conf 

Wireguard is automatically used if you're using either qbittorrent, prowlarr or jackett. Since these either download or index from torrent sites. Just ensure your config is working. Here's a template below that ensures RFC 1918 routes are ignored while internet traffic is routed out the VPN

```
[Interface]
Address = x.x.x.x/32
DNS = 1.1.1.1
PrivateKey = xxxxxx
PostUp = DROUTE=$(ip route | grep default | awk '{print $3}'); HOMENET=192.168.0.0/16; HOMENET2=10.0.0.0/8; HOMENET3=172.16.0.0/12; ip route add $HOMENET3 via $DROUTE; ip route add $HOMENET2 via $DROUTE; ip route add $HOMENET via $DROUTE; iptables -I OUTPUT -d $HOMENET -j ACCEPT; iptables -A OUTPUT -d $HOMENET2 -j ACCEPT; iptables -A OUTPUT -d $HOMENET3 -j ACCEPT;  iptables -A OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT; ip route add 10.10.10.1/32 via 10.10.10.1
PreDown = HOMENET=192.168.0.0/16; HOMENET2=10.0.0.0/8; HOMENET3=172.16.0.0/12; ip route delete $HOMENET; ip route delete $HOMENET2; ip route delete $HOMENET3; iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT; iptables -D OUTPUT -d $HOMENET -j ACCEPT; iptables -D OUTPUT -d $HOMENET2 -j ACCEPT; iptables -D OUTPUT -d $HOMENET3 -j ACCEPT; ip route delete 10.10.10.1

[Peer]
AllowedIPs = 0.0.0.0/0
Endpoint = x.x.x.x:51820
PersistentKeepalive = 60
PublicKey = xxxxx

```
<b>Here's a rough breakdown:<b>

* DROUTE=$(ip route | grep default | awk '{print $3}) is getting the default route via which to send all subsequent traffic.
* HOMENET, HOMENET2, and HOMENET3 define ranges of RFC 1918 - Private Addresses.
* There are a series of ip route add commands adding these networks to the routing table via the existing default gateway route. This is to preserve routing on the local LAN while wireguard does the default route.
* This reject packets going outbound if it's not on the wireguard interface and it's not wireguard ip rule marked and the destination address isn't a local interface<br>```iptables -A OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT```
* This simply adds a route so you can connect to the wireguard servers local address. <br>
```ip route add 10.10.10.1/32 via 10.10.10.1```
* iptables commands following that then enable outbound traffic to these private networks.
* Lastly, ip route delete and iptables commands in PreDown are undoing all these steps when the Docker container is stopped, effectively cleaning up the rules and routes set up to enable networking.

#### Ensure the VPN is working

<span style='color: red;'>!!IMPORTANT!!!</span> Run the below command and verify the IP isn't your own public IP. If it is run ```docker logs wireguard``` to see what's failed with your wg0.conf config file.
```
docker exec -it qbittorrent bash -c "curl ifconfig.me"
```
#### Why Wireguard?
Wireguard is efficient and runs in a connectionless state. If the VPN drops it will not fall back to your address like OpenVPN and useles signficantly less resources. Host it on a VPS or use a trusted VPN provider that offers wireguard,

## Why

### Why use a APIPA 169.254.x.x range?

Using an APIPA range for internal Docker networking is recommended to prevent IP address conflicts and ensure seamless communication between different networks that are defined in RFC 1918. By utilizing the Automatic Private IP Addressing (APIPA) ranges, Docker can dynamically assign IP addresses within a specific range, ensuring that they do not interfere with your existing LAN connections and routes. This approach helps maintain network stability and efficiency, as well as simplifies the management of IP addresses within your Docker environment.

### Why a proxy network?

* Security: By configuring a proxy network, you can add an extra layer of security to your Docker environment. A proxy can act as a barrier between your containers and the outside world, filtering requests and responses and blocking unauthorized access when you expose resources via nginx or traefik reverse proxy.
* Traffic management: A proxy network can help you manage and control the flow of traffic between your containers. This can be particularly useful if you have multiple containerized applications running on the same network, as it allows you to reach the containers by hostname.

# Using SWAG - Requires some manual configuration on Nginx config files
* Why you might prefer Traefik - https://www.linuxserver.io/blog/2018-02-03-using-traefik-as-a-reverse-proxy-with-docker<br>
* Refer to https://docs.linuxserver.io/images/docker-swag/#umask-for-running-applications for documentation<br>
* Once started go into the nginx proxy-confs in the config directory and copy the example to a .conf file. 

# Using Traefik

By default the TLD will be traefik.me is used for local deployment and can be changed in the .env file to your own domain. Please see docker-compose-traefik.yml.example

# Deploying the Stack

### Deployment
```bash
docker-compose -f docker-compose-all.yml up -d
```