#!/usr/bin/env bash
echo "The script you are running has:"
echo "basename: [$(basename "$0")]"
echo "dirname : [$(dirname "$0")]"
echo "pwd     : [$(pwd)]"

read -p "Do you want to proceed with the deletion of config and data? (yes/no) " yn

case $yn in 
	yes ) echo ok, we will proceed;;
	no ) echo exiting...;
		exit;;
	* ) echo invalid response;
		exit 1;;
esac

rm -Rf $(dirname "$0")/data/ $(dirname "$0")/config/
echo "config and data directories deleted"