#!/bin/bash

# Create the directory if it doesn't exist
mkdir -p $(dirname "$0")/ssl

 Download and save the PEM files, overwriting existing ones if necessary
for file in {fullchain,privkey}.pem
do
    curl -o $(dirname "$0")/ssl/$file https://traefik.me/$file
done
chmod 600 $(dirname "$0")/ssl/fullchain.pem
chmod 600 $(dirname "$0")/ssl/privkey.pem
echo "Files saved (or overwritten) in the 'ssl' directory."